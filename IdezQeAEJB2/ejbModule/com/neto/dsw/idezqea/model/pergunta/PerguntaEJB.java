package com.neto.dsw.idezqea.model.pergunta;

import java.util.List;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

@Stateless
public class PerguntaEJB implements QeAPergunta{

	@PersistenceContext
	private EntityManager en;
	
	@Override
	public void adicionarPergunta(Pergunta pergunta) {
		System.out.println("Pergunta salva");
		en.persist(pergunta);
	}

	@Override
	public List<Pergunta> obterPerguntas() {
		System.out.println("obter Perguntas");
		javax.persistence.Query query = en.createQuery("Select p from Pergunta p");
		return query.getResultList();
	}

	@Override
	public void removerPergunta(Pergunta pergunta) {
		System.out.println("remover Pergunta");
		Pergunta perguntaARemover = en.find(Pergunta.class, pergunta.getId());
		en.remove(perguntaARemover);
		
	}

	@Override
	public void atualizarPergunta(Pergunta pergunta) {
		System.out.println("atualizar Perguntas");
		en.merge(pergunta);
	}

	@Override
	public List<Pergunta> pesquisa(String valor) {
		javax.persistence.Query query = en.createQuery("Select p from Pergunta p where p.pergunta like '%"+ valor +"%'");
		return query.getResultList();
	}

}
