package com.neto.dsw.idezqea.model.pergunta;

import java.util.List;

public interface QeAPergunta {

	public void adicionarPergunta( Pergunta pergunta );
	
	public List<Pergunta> pesquisa( String valor );
	
	public List<Pergunta> obterPerguntas();
	
	public void removerPergunta( Pergunta pergunta );
	
	public void atualizarPergunta( Pergunta pergunta );
	
}
