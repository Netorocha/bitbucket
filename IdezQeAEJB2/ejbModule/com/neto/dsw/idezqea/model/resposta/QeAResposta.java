package com.neto.dsw.idezqea.model.resposta;

import java.util.List;


public interface QeAResposta {

	public void adicionarResposta( Resposta resposta );
	
	public List<Resposta> obterRespostas();
	
	public void removerResposta( Resposta resposta );
	
	public void atualizarResposta( Resposta resposta );
	
}
