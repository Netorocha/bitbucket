package com.neto.dsw.idezqea.model.resposta;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class RepostaEJB implements QeAResposta {

	@PersistenceContext
	private EntityManager en;
	
	@Override
	public void adicionarResposta(Resposta resposta) {
		System.out.println("Resposta salva");
		en.persist(resposta);
	}

	@Override
	public List<Resposta> obterRespostas() {
		System.out.println("obter Respostas");
		javax.persistence.Query query = en.createQuery("Select r from Resposta r");
		return query.getResultList();
	}

	@Override
	public void removerResposta(Resposta resposta) {
		System.out.println("remover Resposta");
		Resposta respostaARemover = en.find(Resposta.class, resposta.getId());
		en.remove(respostaARemover);
	}

	@Override
	public void atualizarResposta(Resposta resposta) {
		System.out.println("atualizar Respostas");
		en.merge(resposta);
		
	}

	

}
