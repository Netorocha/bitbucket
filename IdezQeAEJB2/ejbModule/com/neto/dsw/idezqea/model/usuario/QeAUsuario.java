package com.neto.dsw.idezqea.model.usuario;

import java.util.List;

public interface QeAUsuario {
	
	public void adicionarUsuario( Usuario user );
	
	public List<Usuario> obterUsuarios();
	
	public void removerUsuario( Usuario user );
	
	public void atualizarUsuario( Usuario user );
}
