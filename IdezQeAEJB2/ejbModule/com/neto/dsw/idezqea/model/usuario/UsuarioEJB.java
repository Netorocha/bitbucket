package com.neto.dsw.idezqea.model.usuario;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.*;

@Stateless
public class UsuarioEJB implements QeAUsuario{

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public void adicionarUsuario(Usuario user) {
		System.out.println("Add usuario");
		em.persist(user);
	}

	@Override
	public List<Usuario> obterUsuarios() {
		System.out.println("obter usuario");
		Query query = em.createQuery("Select u from Usuario u");
		return query.getResultList();
	}

	@Override
	public void removerUsuario(Usuario user) {
		System.out.println("remover usuario");
		Usuario userARemover = em.find(Usuario.class, user.getId());
		em.remove(userARemover);
	}

	@Override
	public void atualizarUsuario(Usuario user) {
		System.out.println("atualizar usuario");
		em.merge(user);
		
	}

}
