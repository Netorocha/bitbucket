package com.neto.dsw.idezqea.beans;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.neto.dsw.idezqea.model.pergunta.Pergunta;
import com.neto.dsw.idezqea.model.pergunta.QeAPergunta;
import com.neto.dsw.idezqea.model.usuario.Usuario;

@Named
@ConversationScoped
public class PerguntaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Conversation conversation;
	
	@EJB
	private QeAPergunta perguntaQeA;

	private Pergunta pergunta;
	
	private String valor;
	
	public PerguntaBean()
	{
		this.pergunta = new Pergunta();
	}
	
	public Pergunta getPergunta() {
		return pergunta;
	}

	public void setPergunta(Pergunta pergunta) {
		this.pergunta = pergunta;
	}
	
	public String verPergunta()
	{
		if ( conversation.isTransient() ){
			this.conversation.begin();
		}
		return "questions";
	}
	
	public String cadastrarPergunta()
	{
		perguntaQeA.adicionarPergunta(this.pergunta);
		if ( conversation.isTransient() ){
			this.conversation.begin();
		}
		return "questions?faces-redirect=true";
	}
	
	public List<Pergunta> getPerguntas()
	{
		return perguntaQeA.obterPerguntas();
	}
	
	public String prepararPesquisa(String valor){
		this.valor = valor;
		if ( conversation.isTransient() ){
			this.conversation.begin();
		}
		return "pesquisar?faces-redirect=true";
	}
	
	public List<Pergunta> getPesquisa()
	{
		return perguntaQeA.pesquisa(valor);
	}
	
	public String removerPergunta()
	{
		perguntaQeA.removerPergunta(this.pergunta);
		return null;
	}
	
	public String editarPergunta()
	{
		perguntaQeA.atualizarPergunta(this.pergunta);
		if ( ! conversation.isTransient() ){
			conversation.end();
		}
		return "questions";
	}
	
	public String prepararEdicao(Pergunta perguntaAEditar){
		if ( conversation.isTransient() ){
			this.conversation.begin();
		}
		this.pergunta = perguntaAEditar;
		return "editarPergunta?faces-redirect=true";
	}
}
