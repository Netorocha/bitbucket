package com.neto.dsw.idezqea.beans;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.neto.dsw.idezqea.model.resposta.QeAResposta;
import com.neto.dsw.idezqea.model.resposta.Resposta;
import com.neto.dsw.idezqea.model.usuario.Usuario;
import com.neto.dsw.idezqea.model.pergunta.Pergunta;

@Named
@ConversationScoped
public class RespostaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Conversation conversation;
	
	@EJB
	private QeAResposta respostaQeA;
	
	private Pergunta pergunta;
	
	private Usuario usuario;
	
	private Resposta resposta;
	
	public RespostaBean()
	{
		this.resposta = new Resposta();
	}

	public Resposta getResposta() {
		return resposta;
	}

	public void setResposta(Resposta resposta) {
		this.resposta = resposta;
	}
	
	public String cadastrarResposta()
	{
		respostaQeA.adicionarResposta(this.resposta);
		return null+"?faces-redirect=true";
	}
	
	public List<Resposta> getRespostas()
	{
		return respostaQeA.obterRespostas();
	}

	public String removerResposta()
	{
		respostaQeA.removerResposta(this.resposta);
		return null;
	}
	
	public String editarResposta()
	{
		respostaQeA.atualizarResposta(this.resposta);
		if ( ! conversation.isTransient() ){
			conversation.end();
		}
		return "questions";
	}
	
	public String prepararEdicao(Resposta respostaAEditar){
		if ( conversation.isTransient() ){
			this.conversation.begin();
		}
		this.resposta = respostaAEditar;
		return "editarResposta?faces-redirect=true";
	}
	
	public String getUser()
	{
		return usuario.getNome();
	}
	
	public long getPergunta()
	{
		return pergunta.getId();
	}
}
