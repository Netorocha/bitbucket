package com.neto.dsw.idezqea.beans;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import java.util.List;

import com.neto.dsw.idezqea.model.usuario.QeAUsuario;
import com.neto.dsw.idezqea.model.usuario.Usuario;

@Named
@ConversationScoped
public class UsuarioBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Conversation conversation;
	
	@EJB
	private QeAUsuario usarioQeA;

	private Usuario user;
	
	public UsuarioBean()
	{
		this.user = new Usuario();
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}
	
	public List<Usuario> getUsuarios()
	{
		return usarioQeA.obterUsuarios();
	}
	
	public String cadastrarUsuario()
	{
		usarioQeA.adicionarUsuario(this.user);
		return "listarUsuarios?faces-redirect=true";
	}
	
	public String editarUsuario()
	{
		usarioQeA.atualizarUsuario(this.user);
		if ( ! conversation.isTransient() ){
			conversation.end();
		}
		return "listarUsuarios";
	}
	
	public String prepararEdicao(Usuario usuarioAEditar){
		if ( conversation.isTransient() ){
			this.conversation.begin();
		}
		this.user = usuarioAEditar;
		return "editarUsuario?faces-redirect=true";
	}
	
	public String removerUsuario( Usuario user )
	{
		usarioQeA.removerUsuario(user);
		return null;
	}
}
